import React from 'react'
import axiosInstance from './Api/axios'
import AddContactAlert from './AddContactAlert'

class AddContact extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            id : '',
            name : '',
            ph : '',
            email : '',
            address : '',
            img : '',
            isError : ''
        }
    }

    handleOnchange = (event) => {
        const { name, value } = event.target
        this.setState({
            [name] : value
        })
    }


    handleOnSubmit = (event) => {
        event.preventDefault()
        const formData = this.state
        formData.img = 'person.jpg'
        axiosInstance.post(`/contacts`,formData)
        .then((response)=>{
            this.setState({
                isError : false
            })
        })
        .catch((error)=>{
            this.setState({
                isError : true
            })
        })
    }

    render(){
        return(
            <>
                <div className = 'card col-md-6 mx-auto mt-4 bg-custom'>
                    <form onSubmit = {this.handleOnSubmit} className = 'form-group'>
                        <label className='p-1'>Name :</label>
                        <input className = 'form-control' type = 'text' name = 'name' onChange = {this.handleOnchange} required/>

                        <label className='p-1'>Ph :</label>
                        <input className = 'form-control' type = 'number' name = 'ph' onChange = {this.handleOnchange} required pattern = ".{10,}"/>

                        <label className='p-1'>Email :</label>
                        <input className = 'form-control' type = 'email' name = 'email' onChange = {this.handleOnchange} required pattern="[a-z0-9._+%-]+@[a-z0-9]+\.[a-z]{2,}$"/>

                        <label className='p-1'>Address :</label>
                        <input className = 'form-control' type = 'text' name = 'address' onChange = {this.handleOnchange} required/>

                        <button type = 'submit' className = 'btn btn-primary mt-2'>Submit</button>
                    </form>
                </div>
                {this.state.isError === true ? <AddContactAlert addContactMessage = 'Unable to Add! Check the Details' {...this.props} /> : (this.state.isError===false&&<AddContactAlert addContactMessage = 'Contact Added!' {...this.props} />)}
            </>
        )
    }
}
export default AddContact