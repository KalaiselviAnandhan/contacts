import React from "react"
import profile from './images/Blank-profile.png'
import {Link} from 'react-router-dom'
import axiosInstance from './Api/axios'
import {Spinner} from 'react-bootstrap'

class ContactList extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            contactData : [],
            loading : true
        }
    }

    getAllContacts = () => {
        axiosInstance.get('/contacts')
        .then((response)=>{
            this.setState({
                contactData : response.data,
                loading : false
            })
        })
        .catch((error)=>console.error(error))
    }

    componentDidMount(){
        this.setState({
            loading : true
        })
        this.getAllContacts()
    }

    render(){
    const contactlist = this.state.contactData.length === 0 ? <div><h3>No more Contacts</h3></div> : this.state.contactData.map((item)=><div key={item.id} className='bg-list m-2 rounded'><Link to ={`/contact/${item.id}`}><div className='d-flex align-items-center' data-toggle='tool-tip' data-placement='top' title='Click to View Contact Info' ><img src={profile}  className='img-fluid rounded-circle' width='50' height='50' alt='profile'/><h3  className='ml-2 font-weight-normal'>{item.name}</h3></div></Link></div>)
        return(
            <>
                { this.state.loading ? 
                <Spinner animation = 'border' variant = 'info' className='mt-5 m-2 mx-auto' /> :
                <div className='card col-sm-5 mt-5 m-2 shadow p-3 bg-custom rounded'> { contactlist }</div> 
                }
            </>
        )
    }
}
export default ContactList