import React from 'react'
import './App.css'
import ContactList from './ContactList'
import {Route} from "react-router-dom"
import ContactInfo from './ContactInfo'
import Header from './Header'
import AddContact from './AddContact'

import Media from 'react-media'

function App() {
  return (
    <div className='container'>
      <Route path='/'><Header/></Route>
      <Route exact path='/add' component={AddContact}/>
      <Media queries={{ small: { maxWidth: 599 } }}>
          {matches =>
            matches.small ? (
              <div className='row justify-content-center'>
                <Route exact path='/contact'><ContactList/></Route>
                <Route exact path='/contact/:contact_id' component={ContactInfo} />
              </div>
            ) : (
              <div className='row justify-content-center'>
                <Route  path='/contact'><ContactList/></Route>
                <Route exact path='/contact/:contact_id' component={ContactInfo} />
            </div>
            )
          }
        </Media>
    </div>
  );
}

export default App;
