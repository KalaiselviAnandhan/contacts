import React from 'react'
import {Link} from "react-router-dom"
import {Navbar, Nav} from 'react-bootstrap'
import {FaRegAddressBook, FaUserPlus, FaUsers, FaHome} from 'react-icons/fa'

function Header(){
    return(
        <Navbar className='bg-custom' expand='lg'>
            <Navbar.Brand><h1 className='text-white' data-toggle='tool-tip' data-placement='bottom' title='Home'><FaRegAddressBook className='mb-1'style={{color:'gray'}}/> Contacts</h1></Navbar.Brand>
            <Navbar.Toggle aria-controls = 'navigation' className='border-secondary'/>
            <Navbar.Collapse id = 'navigation'>
                <Nav className='ml-auto'>
                    <Link to='/' className='nav-link'><h2 className='text-white' data-toggle='tool-tip' data-placement='bottom' title='All Contact'><FaHome className='mr-1'style={{color:'gray'}}/> Home</h2></Link>
                    <Link to='/add' className='nav-link'><h2 className='text-white' data-toggle='tool-tip' data-placement='bottom' title='Add New Contact'><FaUserPlus className='mr-1'style={{color:'gray'}}/> New</h2></Link>
                    <Link to='/contact' className='nav-link'><h2 className='text-white' data-toggle='tool-tip' data-placement='bottom' title='All Contact'><FaUsers className='mr-1'style={{color:'gray'}}/> All</h2></Link>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}

export default Header