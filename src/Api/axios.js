import axios from 'axios'
const BaseURL = process.env.NODE_ENV === "production" ? "https://kalai-anand-contacts-backend.herokuapp.com" : "https://kalai-anand-contacts-backend.herokuapp.com"

const axiosInstance = axios.create({
    baseURL: BaseURL
})

export default axiosInstance