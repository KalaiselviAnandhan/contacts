import React from "react"
import person from './images/person.jpg'
import {FaPhoneAlt, FaUserCircle, FaCity, FaArrowLeft, FaEnvelope, FaUserEdit, FaUserMinus} from 'react-icons/fa'
import axiosInstance from './Api/axios'
import {Modal, Button} from 'react-bootstrap'
import ContactEditAlert from './ContactEditAlert'

class ContactInfo extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            show : false,
            id : '',
            name : '',
            ph : '',
            email : '',
            address : '',
            img : '',
            isEditError : '',
            isDeleteShow : false,
            showDeleteModal : false,
        }
    }

    handleOnchange = (event) => {
        const { name, value } = event.target
        this.setState({
            [name] : value
        })
    }

    handleHide = () => {
        this.setState({show:false})
    }

    handleModalHide = () => {
        this.setState({showDeleteModal:false})
    }

    getContactInfo = () => {
        axiosInstance.get(`/contacts/${this.props.match.params.contact_id}`)
        .then((response)=>{
            this.setState({
                id: response.data[0].id,
                name: response.data[0].name,
                ph: response.data[0].ph,
                email: response.data[0].email,
                address: response.data[0].address,
                img: response.data[0].img
            })
        })
        .catch((error)=>console.error(error))
    }

    handleOnSubmit = (event) => {
        event.preventDefault()
        const formData = this.state
        axiosInstance.put(`/contacts/${this.state.id}`,formData)
        .then((response) => {
            this.handleHide()
            this.setState({isEditError:false})
        })
        .catch((error)=>this.setState({isEditError:true}))
    }

    handleOnclick = () => {
        this.props.history.push('/contact')
    }

    handleDelete = (event) => {
        this.setState({
            isDeleteShow : true,
            showDeleteModal : true
        })
    }

    handleModalConfirm = () => {
        const id = this.state.id
        axiosInstance.delete(`/contacts/${id}`)
        .then((response) => {
            this.handleModalHide()
            this.props.history.push('/')
            this.props.history.push('/contact')
        })
        .catch((error) => console.log(error))
    }

    componentDidMount(){
        this.getContactInfo()
    }


    render(){
        
        return(
            <>
                <div className='card col-sm-5  m-2 mt-5 mb-1 shadow p-3 bg-custom rounded'>
                    <div className='d-flex flex-row justify-content-around align-items-center'>
                        <FaArrowLeft onClick={this.handleOnclick} data-toggle='tool-tip' data-placement='bottom' title='Go Back' style = {{cursor:'pointer'}} />
                        <h2>Contact Info</h2>
                        <FaUserEdit className='custom-font-size' data-toggle='tool-tip' data-placement='bottom' title='Edit Contact Info' style = {{cursor:'pointer'}} onClick={()=>this.setState({show:true})}/>
                        <FaUserMinus className='custom-font-size' data-toggle='tool-tip' data-placement='bottom' title='Delete Contact' style = {{cursor:'pointer'}} onClick = {this.handleDelete}/>

                    </div>
                    <div className='d-flex flex-column'>
                        <img src={person} className='img-fluid rounded-circle border border-0 m-2' alt='profile'/>
                        <ul className="list-group mt-2 text-center">
                            <li className='list-group-item border-0 shadow-sm p-3 bg-list mb-2 rounded font-weight-normal'><FaUserCircle style={{color:'gray'}}/> {this.state.name}</li>
                            <li className='list-group-item border-0 shadow-sm p-3 bg-list mb-2 rounded font-weight-normal'><FaPhoneAlt style={{color:'gray'}}/> {this.state.ph}</li>
                            <li className='list-group-item border-0 shadow-sm p-3 bg-list mb-2 rounded font-weight-normal'><FaEnvelope style={{color:'gray'}}/> {this.state.email}</li>
                            <li className='list-group-item border-0 shadow-sm p-3 bg-list mb-2 rounded font-weight-normal'><FaCity style={{color:'gray'}}/> {this.state.address}</li>
                        </ul>
                    </div>
                </div>
                
                <Modal show={this.state.show} onHide = {this.handleHide} size = 'md' centered>
                    <Modal.Header closeButton>
                        <Modal.Title className = 'text-center'>Edit Contact Info</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <form onSubmit = {this.handleOnSubmit} className = 'form-group'>
                            <label>Name :</label>
                            <input className = 'form-control' type = 'text' name = 'name' value = {this.state.name} onChange = {this.handleOnchange} required/>

                            <label>Ph :</label>
                            <input className = 'form-control' type = 'number' name = 'ph' value = {this.state.ph} onChange = {this.handleOnchange} required pattern = ".{10,}"/>

                            <label>Email :</label>
                            <input className = 'form-control' type = 'email' name = 'email' value = {this.state.email} onChange = {this.handleOnchange} required pattern="[a-z0-9._+%-]+@[a-z0-9]+\.[a-z]{2,}$"/>

                            <label>Address :</label>
                            <input className = 'form-control' type = 'text' name = 'address' value = {this.state.address} onChange = {this.handleOnchange} required/>

                            <button type = 'submit' className = 'btn btn-primary mt-2'>Submit</button>
                        </form>
                    </Modal.Body>
                </Modal>

                {this.state.isEditError === true ? <ContactEditAlert editMessage = 'Unable to Edit! Check the Details' {...this.props} /> : (this.state.isEditError===false&&<ContactEditAlert editMessage = 'Contact Details Updated!' {...this.props} />)}

                {this.state.isDeleteShow && 
                <Modal show={this.state.showDeleteModal}>
                    <Modal.Header closeButton onClick={this.handleModalHide}>
                        <Modal.Title>Contacts</Modal.Title>
                    </Modal.Header>
                    <Modal.Body> <h5> Would You Like to Delete Contact ?</h5> </Modal.Body>
                    <Modal.Footer>
                        <Button variant="primary" onClick={this.handleModalHide}>
                            No
                        </Button>
                        <Button variant="primary" onClick={this.handleModalConfirm}>
                            Yes
                        </Button>
                    </Modal.Footer>
                </Modal>
                }
            </>
        )
    }
}
export default ContactInfo